__Данный сервис принимает все запросы свзяанные с order.__

- `save`

http://130.193.53.230:8989/order/save `POST` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)

- Params
    - (key) `nameDepartment` (value) `"name department"` (Обязательный)

- body(JSON)
    - (key) `address_to:` (value) `custom address_to` (Обязательный)
    - (key) `address_from:` (value) `custom address_from` (Обязательный)
    - (key) `description":` (value) `custom description"` (Обязательный)
    - (key) `phone":` (value) `format +1-111-111-11-11"` (Обязательный)
    - (key) `name_client":` (value) `custom name_client"` (Обязательный)

`> http://130.193.53.230:8989/order/save?nameDepartment=Test`

```
{
    "address_to": "1",
    "address_from": "2",
    "description": "Test",
    "phone": "+1-111-111-11-11",
    "name_client": "Test"
}
```

> Данный url сохраняет order. Время в часовом поясе UTC+0.
---
---
---

- `delete`

http://130.193.53.230:8989/order/delete `DELETE` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)

- Params
    - (key) `create_time` (value) `"format date yyyy-MM-dd HH:mm"` (Обязательный)
    - (key) `phone` (value) `"phone format +1-111-111-11-11"` (Обязательный)

`> http://130.193.53.230:8989/order/delete?create_time=2022-07-25 16:03&phone=%2b1-111-111-11-11`

> Данный url удаляет заказ по дате создания заказа и номеру телефона. Время в Часовом поясе UTC+0.
---
---
---

- `finished`

http://130.193.53.230:8989/order/finished `POST` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)

- Params
    - (key) `create_time` (value) `"format date yyyy-MM-dd HH:mm"` (Обязательный)
    - (key) `phone` (value) `"phone format +1-111-111-11-11"` (Обязательный)

`> http://130.193.53.230:8989/order/finished?create_time=2022-07-16 21:32&phone=%2b1-111-111-11-11`

> Данный url завершает order. Время в Часовом поясе UTC+0.
---
---
---

- `all-completed`

http://130.193.53.230:8989/order/all-completed `GET` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)

- Params
    - (key) `nameDepartment` (value) `"nameDepartment"` (Обязательный)

`> http://130.193.53.230:8989/order/all-completed?nameDepartment=Test`

> Данный url возвращает завершенные заказы компании`.
---
---
---

- `take`

http://130.193.53.230:8989/order/take `POST` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)
`Требуется MYSESSION`[<sup>получить MYSESSION</sup>](#f2)

- Params
    - (key) `create_time` (value) `"format date yyyy-MM-dd HH:mm"` (Обязательный)
    - (key) `phone` (value) `"phone format +1-111-111-11-11"` (Обязательный)

`> http://130.193.53.230:8989/order/take?create_time=2022-07-25 16:11&phone=%2b1-111-111-11-11`

> Данный url назначает заказа водителю по session. Время в Часовом поясе UTC+0.
---
---
---

- `show`

http://130.193.53.230:8989/order/show `POST` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)
`Требуется MYSESSION`[<sup>получить MYSESSION</sup>](#f2)

- Params
    - (key) `create_time` (value) `"format date yyyy-MM-dd HH:mm"` (Обязательный)
    - (key) `phone` (value) `"phone format +1-111-111-11-11"` (Обязательный)

`> http://130.193.53.230:8989/order/show?create_time=2022-07-16 22:01&phone=%2b1-111-111-11-11`

> Данный url назначает заказа водителю по session`. Время в Часовом поясе UTC+0.
---
---
---
- <span id="f1">авторизация в postman</span> :

> Переходим в вкладку `authorization`

<img src="images/authorization.png" width="800" height="400">

> В Type выбираем `basic auth`

<img src="images/basicAuth1.png" width="800" height="400">

> В поле `Username` и `Password` вводим ранее зарегистрированные данные

<img src="images/basicAuth.png" width="800" height="400">

---
---
---

- <span id="f2">получить MYSESSION</span> :

> - переходим на форму login http://130.193.53.230:8989/login
> - открываем панель разработчика в браузере и переходим в вкладку сеть
> - вводим учетные данные ранее зарегистрированы и входи в систему

<img src="images/login.png" width="800" height="400">

> после авторизации в вкладке сеть переходим на host сайта и в Set-Cookie находим MYSESSION="Session"
> копируем (MYSESSION="Session")

<img src="images/session.png" width="800" height="400">

> переходи в headers в postman

<img src="images/headers.png" width="800" height="400">

> добавляем (key) Cookie (value) MYSESSION="Session" ранние скопированный.

<img src="images/cookie.png" width="800" height="400">