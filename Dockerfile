FROM openjdk:11-jre
#Открываем порт в docker container
EXPOSE 8077
#Создаем переменную которая хранит путь до jar файла
ARG JAR_FILE=build/libs/order-1.0.jar
#Копируем jar файл в docker container
ADD ${JAR_FILE} order.jar
#Запускам jar файл
ENTRYPOINT ["java","-jar","order.jar"]