package com.test.order.service;

import com.test.order.exception.DepartmentException;
import com.test.order.exception.DriverException;
import com.test.order.exception.OrderException;
import com.test.order.repository.CompanyRepository;
import com.test.order.repository.DriverRepository;
import com.test.order.repository.MySessionRepository;
import com.test.order.repository.OrderRepository;
import db.entity.Department;
import db.entity.Driver;
import db.entity.MySession;
import db.entity.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private CompanyRepository companyRepository;

    @Mock
    private MySessionRepository mySessionRepository;

    @Mock
    private DriverRepository driverRepository;

    @InjectMocks
    private OrderService orderService;

    @Test
    void save_exNotIsNameDepartmentInDataBase() {
        Order order = new Order();
        String nameDepartment = "Test";

        when(companyRepository.findByName(Mockito.anyString()))
                .thenReturn(Optional.empty());


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> orderService.save(order, nameDepartment));


        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void save_exOrderExistsInDataBase() {
        String nameDepartment = "Test";
        Order order = new Order();
        order.setPhone("+7-777-777-77-77");
        Department department = new Department();
        department.setName("Test");

        when(companyRepository.findByName(Mockito.anyString()))
                .thenReturn(Optional.of(department));
        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.of(order));


        OrderException orderException = assertThrows(OrderException.class,
                () -> orderService.save(order, nameDepartment));


        assertEquals(OrderException.class, orderException.getClass());
    }

    @Test
    void save_ok() {
        String nameDepartment = "Test";
        Department department = new Department();
        department.setName("Test");
        Order order = new Order();
        order.setPhone("+7-777-777-77-77");
        order.setDepartment(department);

        when(companyRepository.findByName(Mockito.anyString()))
                .thenReturn(Optional.of(department));
        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.empty());
        when(orderRepository.save(any(Order.class)))
                .thenReturn(order);


        Order orderActual = orderService.save(order, nameDepartment);


        assertEquals(order.getPhone(), orderActual.getPhone());
        assertEquals(department.getName(), orderActual.getDepartment().getName());
    }

    @Test
    void deleteOrder_exNotOrderInDataBase() {
        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.empty());

        OrderException orderException = assertThrows(OrderException.class,
                () -> orderService.deleteOrder("2022-07-16 21:31", "+7-777-777-77-77"));

        assertEquals(OrderException.class, orderException.getClass());
    }

    @Test
    void deleteOrder_ok() {
        Order order = new Order();
        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.of(order));


        boolean isDelete = orderService.deleteOrder("2022-07-16 21:31", "+7-777-777-77-77");


        assertTrue(isDelete);
    }


    @Test
    void showAllCompletedOrders_exNotNameDepartmentInDataBase() {
        String nameDepartment = "Test";

        when(companyRepository.findByName(anyString()))
                .thenReturn(Optional.empty());

        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> orderService.showAllCompletedOrders(nameDepartment));


        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void showAllCompletedOrders_ok() {
        String nameDepartment = "Test";
        Department department = new Department();

        when(companyRepository.findByName(anyString()))
                .thenReturn(Optional.of(department));
        when(orderRepository.findAllByDepartmentAndStatus(anyString(), any()))
                .thenReturn(List.of(new Order(), new Order()));


        List<Order> ordersListActual = orderService.showAllCompletedOrders(nameDepartment);


        assertEquals(2, ordersListActual.size());
    }


    @Test
    void finishedOrder_exNotOrderInDataBase() {
        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.empty());


        OrderException orderException = assertThrows(OrderException.class,
                () -> orderService.deleteOrder("2022-07-16 21:31", "+7-777-777-77-77"));


        assertEquals(OrderException.class, orderException.getClass());
    }

    @Test
    void finishedOrder_exNotDriverInOrder() {
        Order order = new Order();
        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.of(order));


        OrderException orderException = assertThrows(OrderException.class,
                () -> orderService.finishedOrder("2022-07-16 21:31", "+7-777-777-77-77"));


        assertEquals(OrderException.class, orderException.getClass());
    }

    @Test
    void finishedOrder_ok() {
        Order order = new Order();
        order.setTimeCreateOrder("2022-07-16 21:31");
        order.setPhone("+7-777-777-77-77");
        Driver driver = new Driver();
        order.setDriver(driver);

        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.of(order));
        when(orderRepository.save(any(Order.class)))
                .thenReturn(order);


        Order orderActual = orderService.finishedOrder(order.getTimeCreateOrder(), order.getPhone());


        assertEquals(order.getTimeCreateOrder(), orderActual.getTimeCreateOrder());
    }

    @Test
    void takeOrder_exNotOrderInDataBase() {
        String dataCreate = "2022-07-16 21:31";
        String phone = "+7-777-777-77-77";
        String session = "1Km2Lx08Knsj";
        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.empty());


        OrderException orderException = assertThrows(OrderException.class,
                () -> orderService.takeOrder(dataCreate, phone, session));


        assertEquals(OrderException.class, orderException.getClass());
    }

    @Test
    void takeOrder_exExistDriverInOrder() {
        String dataCreate = "2022-07-16 21:31";
        String phone = "+7-777-777-77-77";
        String session = "1Km2Lx08Knsj";

        Order order = new Order();
        Driver driver = new Driver();
        order.setDriver(driver);

        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.of(order));


        OrderException orderException = assertThrows(OrderException.class,
                () -> orderService.takeOrder(dataCreate, phone, session));


        assertEquals(OrderException.class, orderException.getClass());
    }

    @Test
    void takeOrder_exNotExistSession() {
        String dataCreate = "2022-07-16 21:31";
        String phone = "+7-777-777-77-77";
        String session = "1Km2Lx08Knsj";

        Order order = new Order();

        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.of(order));
        when(mySessionRepository.findBySessionId(anyString()))
                .thenReturn(Optional.empty());


        DriverException driverException = assertThrows(DriverException.class,
                () -> orderService.takeOrder(dataCreate, phone, session));


        assertEquals(DriverException.class, driverException.getClass());
    }

    @Test
    void takeOrder_exDriveCountOrderOverOne() {
        String dataCreate = "2022-07-16 21:31";
        String phone = "+7-777-777-77-77";

        Order order = new Order();
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");
        MySession mySession = new MySession();
        mySession.setSessionId("1Km2Lx08Knsj");
        mySession.setPrincipalName("Test@mail.ru");

        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.of(order));
        when(mySessionRepository.findBySessionId(anyString()))
                .thenReturn(Optional.of(mySession));
        when(driverRepository.findByEmail(anyString()))
                .thenReturn(Optional.of(driver));
        when(orderRepository.ifDriverOrderProgress(anyString(), any()))
                .thenReturn(1L);


        DriverException driverException = assertThrows(DriverException.class,
                () -> orderService.takeOrder(dataCreate, phone, mySession.getSessionId()));


        assertEquals(DriverException.class, driverException.getClass());
    }

    @Test
    void takeOrder_ok() {
        Order order = new Order();
        order.setTimeCreateOrder("2022-07-16 21:31");
        order.setPhone("+7-777-777-77-77");
        Driver driver = new Driver();
        driver.setEmail("Test@mail.ru");
        MySession mySession = new MySession();
        mySession.setSessionId("1Km2Lx08Knsj");
        mySession.setPrincipalName("Test@mail.ru");

        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.of(order));
        when(mySessionRepository.findBySessionId(anyString()))
                .thenReturn(Optional.of(mySession));
        when(driverRepository.findByEmail(anyString()))
                .thenReturn(Optional.of(driver));
        when(orderRepository.ifDriverOrderProgress(anyString(), any()))
                .thenReturn(0L);
        when(orderRepository.save(any(Order.class)))
                .thenReturn(order);


        Order orderActual = orderService.takeOrder(order.getTimeCreateOrder(), order.getPhone(), mySession.getSessionId());


        assertEquals(orderActual.getTimeCreateOrder(), orderActual.getTimeCreateOrder());
    }


    @Test
    void showOrderFull_exNotOrderInDataBase() {
        String dataCreate = "2022-07-16 21:31";
        String phone = "+7-777-777-77-77";

        when(orderRepository.findByTimeAndPhone(any(), anyString()))
                .thenReturn(Optional.empty());


        OrderException orderException = assertThrows(OrderException.class,
                () -> orderService.showOrderFull(dataCreate, phone));


        assertEquals(OrderException.class, orderException.getClass());
    }

    @Test
    void showOrderFull_ok() {
        Order order = new Order();
        order.setTimeCreateOrder("2022-07-16 21:31");
        order.setPhone("+7-777-777-77-77");

        when(orderRepository.findByTimeAndPhone(any(), anyString()))
                .thenReturn(Optional.of(order));
        when(orderRepository.save(any(Order.class)))
                .thenReturn(order);

        Order orderActual = orderService.showOrderFull(order.getTimeCreateOrder(), order.getPhone());


        assertEquals(order.getTimeCreateOrder(), orderActual.getTimeCreateOrder());
    }

    @Test
    void updateOrder_exNotOrderInDataBase() {
        Order order = new Order();
        order.setTimeCreateOrder("2022-07-16 21:31");
        order.setPhone("+7-777-777-77-77");

        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.empty());

        OrderException orderException = assertThrows(OrderException.class,
                () -> orderService.updateOrder(order));


        assertEquals(OrderException.class, orderException.getClass());
    }

    @Test
    void updateOrder_ok() {
        Order order = new Order();
        order.setTimeCreateOrder("2022-07-16 21:31");
        order.setPhone("+7-777-777-77-77");

        when(orderRepository.findByTimeCreateOrderAndPhone(any(), anyString()))
                .thenReturn(Optional.of(order));
        when(orderRepository.save(any(Order.class)))
                .thenReturn(order);


        boolean isSaveOrder = orderService.updateOrder(order);


        assertTrue(isSaveOrder);
    }
}