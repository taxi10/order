package com.test.order.model.restDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class DriverDTO {

    private String name;

    private String email;

    private String nameDepartment;


}
