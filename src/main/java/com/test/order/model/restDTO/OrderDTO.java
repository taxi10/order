package com.test.order.model.restDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class OrderDTO {

    @JsonProperty(value = "name_client")
    private String nameClient;

    private String description;

    @JsonProperty(value = "time_create_order")
    private String timeCreateOrder;

    @JsonProperty(value = "time_completion_order")
    private String timeCompletionOrder;

    private String status;

    @JsonProperty(value = "address_to")
    private String addressTo;

    @JsonProperty(value = "address_from")
    private String addressFrom;

    private String phone;

    @JsonProperty(value = "email_driver")
    private String emailDriver;

    @JsonProperty(value = "name_department")
    private String nameDepartment;

}
