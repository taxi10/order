package com.test.order.model.exModel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@Builder
public class ExModel {

    private String nameError;

    private String messages;

    private HttpStatus httpStatus;

}
