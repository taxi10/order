package com.test.order.repository;

import db.entity.Order;
import enam.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {

    Optional<Order> findByTimeCreateOrderAndPhone(String timeOrder, String phone);

    @Query("select ord from Order ord left join ord.driver dri on ord.driver.id = dri.id" +
            " where ord.department.name = :nameDepartment and ord.status = :status")
    List<Order> findAllByDepartmentAndStatus(@Param(value = "nameDepartment") String nameDepartment, @Param(value = "status") OrderStatus status);

    @Query("select count(ord) from Order ord where ord.driver.email = :email and ord.status = :status")
    Long ifDriverOrderProgress(@Param(value = "email") String emailDriver,
                               @Param(value = "status") OrderStatus orderStatus);

    @Query("select ord from Order ord " +
            "left join Driver dri on ord.driver.id = dri.id " +
            "left join ord.department dep on ord.department.id = dep.id " +
            "where ord.timeCreateOrder = :time and ord.phone = :phone")
    Optional<Order> findByTimeAndPhone(@Param(value = "time") String timeOrder,
                                       @Param(value = "phone") String phone);

}
