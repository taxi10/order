package com.test.order.repository;

import db.entity.MySession;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MySessionRepository extends JpaRepository<MySession, String> {

    Optional<MySession> findBySessionId(String sessionId);
}
