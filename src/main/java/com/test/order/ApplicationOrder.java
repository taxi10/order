package com.test.order;

import db.entity.Order;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
@EnableDiscoveryClient
@EntityScan(basePackageClasses = {Order.class})
public class ApplicationOrder {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationOrder.class, args);
    }
}
