package com.test.order.util.mapping.Order;

import com.test.order.model.restDTO.OrderDTO;
import db.entity.Order;

import java.util.List;
import java.util.stream.Collectors;

public class OrderToOrderDTO {

    public static List<OrderDTO> convertingOrderListToOrderListDTO(List<Order> orderList) {
        return orderList.stream().map(order -> {
            return OrderDTO.builder()
                    .addressFrom(order.getAddressFrom())
                    .addressTo(order.getAddressTo())
                    .description(order.getDescription())
                    .emailDriver(order.getDriver() != null ? order.getDriver().getEmail() : null)
                    .timeCreateOrder(order.getTimeCreateOrder())
                    .timeCompletionOrder(order.getTimeCompletionOrder())
                    .nameClient(order.getNameClient())
                    .phone(order.getPhone())
                    .status(order.getStatus().getStatus())
                    .nameDepartment(order.getDepartment().getName())
                    .build();
        }).collect(Collectors.toList());
    }

    public static OrderDTO convertingOrderToOrderDTO(Order order) {
        return OrderDTO.builder()
                .phone(order.getPhone())
                .status(order.getStatus().getStatus())
                .nameClient(order.getNameClient())
                .timeCreateOrder(order.getTimeCreateOrder())
                .timeCompletionOrder(order.getTimeCompletionOrder())
                .description(order.getDescription())
                .addressTo(order.getAddressTo())
                .addressFrom(order.getAddressFrom())
                .emailDriver(order.getDriver() != null ? order.getDriver().getEmail() : null)
                .nameDepartment(order.getDepartment().getName())
                .build();
    }

}
