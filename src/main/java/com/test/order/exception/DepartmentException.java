package com.test.order.exception;

public class DepartmentException extends RuntimeException {
    public DepartmentException(String message) {
        super(message);
    }
}
