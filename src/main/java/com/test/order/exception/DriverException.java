package com.test.order.exception;

public class DriverException extends RuntimeException {
    public DriverException(String message) {
        super(message);
    }
}
