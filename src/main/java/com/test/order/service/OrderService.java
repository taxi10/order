package com.test.order.service;

import com.test.order.exception.DepartmentException;
import com.test.order.exception.DriverException;
import com.test.order.exception.OrderException;
import com.test.order.repository.CompanyRepository;
import com.test.order.repository.DriverRepository;
import com.test.order.repository.MySessionRepository;
import com.test.order.repository.OrderRepository;
import db.entity.Department;
import db.entity.Driver;
import db.entity.MySession;
import db.entity.Order;
import enam.OrderStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;

    private final CompanyRepository companyRepository;

    private final MySessionRepository mySessionRepository;

    private final DriverRepository driverRepository;

    @Transactional
    public Order save(Order order, String nameDepartment) {
        Department department = companyRepository.findByName(nameDepartment)
                .orElseThrow(() -> new DepartmentException("There is no department with that name: " + nameDepartment));

        String date = getDate();

        Optional<Order> isNullOrder = orderRepository.findByTimeCreateOrderAndPhone(date, order.getPhone());

        if (isNullOrder.isPresent()) {
            throw new OrderException("An order with such data already exists, try ordering again in a minute.");
        }

        department.setOrderList(List.of(order));
        order.setTimeCreateOrder(date);
        order.setStatus(OrderStatus.EXPECTATION);
        order.setDepartment(department);

        return orderRepository.save(order);
    }

    @Transactional
    public boolean deleteOrder(String timeOrder, String phone) {
        Order order = orderRepository.findByTimeCreateOrderAndPhone(timeOrder, phone)
                .orElseThrow(() -> new OrderException("There is no such order with an id: " +
                        "Date: " + timeOrder + " " + "Number: " + phone));

        order.setStatus(OrderStatus.INVALID);
        orderRepository.save(order);
        return true;
    }

    @Transactional
    public List<Order> showAllCompletedOrders(String nameDepartment) {
        companyRepository.findByName(nameDepartment)
                .orElseThrow(() -> new DepartmentException("There is no department with that name:" + nameDepartment));
        return orderRepository.findAllByDepartmentAndStatus(nameDepartment, OrderStatus.FINISHED);
    }

    @Transactional
    public Order finishedOrder(String timeOrder, String phone) {
        Order order = orderRepository.findByTimeCreateOrderAndPhone(timeOrder, phone)
                .orElseThrow(() -> new OrderException("There is no such order with an id: " +
                        "Date: " + timeOrder + " " + "Number: " + phone));

        Optional.ofNullable(order.getDriver())
                .orElseThrow(() -> new OrderException("The order cannot be completed, the driver has not been assigned."));

        order.setStatus(OrderStatus.FINISHED);
        order.setTimeCompletionOrder(getDate());

        return orderRepository.save(order);
    }

    @Transactional
    public Order takeOrder(String timeOrder, String phone, String sessionId) {
        Order order = orderRepository.findByTimeCreateOrderAndPhone(timeOrder, phone)
                .orElseThrow(() -> new OrderException("There is no such order with an id: " + "Number: " + phone));

        if (order.getDriver() != null) {
            throw new OrderException("The order is executed by another driver");
        }

        MySession mySession = mySessionRepository.findBySessionId(sessionId)
                .orElseThrow(() -> new DriverException("Session error"));

        Driver driver = driverRepository.findByEmail(mySession.getPrincipalName())
                .orElseThrow(() -> new DriverException("There is no driver with this email: " + mySession.getPrincipalName()));

        Long aLong = orderRepository.ifDriverOrderProgress(driver.getEmail(), OrderStatus.PROGRESS);

        if (aLong > 0) {
            throw new DriverException("Complete all previous orders");
        }

        order.setStatus(OrderStatus.PROGRESS);
        driver.setOrderFinished(List.of(order));
        order.setDriver(driver);

        return orderRepository.save(order);
    }

    @Transactional
    public Order showOrderFull(String timeOrder, String phone) {
        return orderRepository.findByTimeAndPhone(timeOrder, phone)
                .orElseThrow(() -> new OrderException("There is no order with such a phone"));
    }

    @Transactional
    public boolean updateOrder(Order order) {
        Order orderActual = orderRepository.findByTimeCreateOrderAndPhone(order.getTimeCreateOrder(), order.getPhone())
                .orElseThrow(() -> new OrderException("There is no order with such a phone"));
        order.setId(orderActual.getId());
        order.setDepartment(orderActual.getDepartment());
        orderRepository.save(order);
        return true;
    }

    private String getDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.now().format(formatter);
    }
}
