package com.test.order.rest;


import com.test.order.model.restDTO.OrderDTO;
import com.test.order.service.OrderService;
import com.test.order.util.mapping.Order.OrderToOrderDTO;
import db.entity.Order;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequiredArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    @PostMapping(value = "/save")
    public ResponseEntity<?> saveOrder(@Valid @RequestBody Order order,
                                       @RequestParam(value = "nameDepartment") String nameDepartment) {
        orderService.save(order, nameDepartment.trim());
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteOrder(@RequestParam(value = "create_time") String timeOrder,
                                         @RequestParam(value = "phone") String phone) {
        orderService.deleteOrder(timeOrder.trim(), phone.trim());
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/finished")
    public ResponseEntity<?> finishedOrder(@RequestParam(value = "create_time") String timeOrder,
                                           @RequestParam(value = "phone") String phone) {
        orderService.finishedOrder(timeOrder, phone);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/all-completed")
    public ResponseEntity<?> getAllCompletedOrders(@RequestParam(value = "nameDepartment") String nameDepartment) {
        List<Order> orders = orderService.showAllCompletedOrders(nameDepartment);
        List<OrderDTO> orderDTOS = OrderToOrderDTO.convertingOrderListToOrderListDTO(orders);
        return ResponseEntity.ok().body(orderDTOS);
    }

    @PostMapping(value = "/take")
    public ResponseEntity<?> takeOrder(@RequestParam(value = "create_time") String timeOrder,
                                       @RequestParam(value = "phone") String phone,
                                       HttpServletRequest httpRequest) {
        String jsessionid = Arrays.stream(httpRequest.getCookies())
                .filter(cookie -> cookie.getName().equals("MYSESSION"))
                .map(cookie -> cookie.getValue())
                .collect(Collectors.joining());
        orderService.takeOrder(timeOrder, phone, jsessionid);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/show")
    public ResponseEntity<?> showOrder(@RequestParam(value = "create_time") String timeOrder,
                                       @RequestParam(value = "phone") String phone) {
        Order order = orderService.showOrderFull(timeOrder, phone);
        OrderDTO orderDTO = OrderToOrderDTO.convertingOrderToOrderDTO(order);
        return ResponseEntity.ok().body(orderDTO);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateOrder(@Valid @RequestBody Order order) {
        orderService.updateOrder(order);
        return ResponseEntity.ok().build();
    }

}
